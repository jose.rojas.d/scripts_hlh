"""Partial-Mesh topology



Five directly connected switches plus a host for each access switch. 



Adding the 'topos' dict with a key/value pair to generate our newly defined

topology enables one to pass in '--topo=mytopo' from the command line.

"""



from mininet.topo import Topo

from mininet.link import TCLink



class MyTopo( Topo ):

    "Partial-mesh topology."



    def build( self ):

        "Create custom topo."



        # Add hosts and switches

        h1 = self.addHost( 'h1', mac="00:00:00:00:00:01", ip='10.0.0.1/24')

        h2 = self.addHost( 'h2', mac="00:00:00:00:00:02", ip='10.0.0.2/24') 

        h3 = self.addHost( 'h3', mac="00:00:00:00:00:03", ip='10.0.0.3/24')

        h4 = self.addHost( 'h4', mac="00:00:00:00:00:04", ip='10.0.0.4/24') 

        ## core

        s1 = self.addSwitch( 's1' )

        ## aggregation

        s2 = self.addSwitch( 's2' )

        s3 = self.addSwitch( 's3' )

        ## access

        s4 = self.addSwitch( 's4' )

        s5 = self.addSwitch( 's5' )



         # Add links

        ## aggregation - core links

        self.addLink( s2, s1, cls=TCLink, bw=10, latency_ms=5, delay='5ms' )

        self.addLink( s3, s1, cls=TCLink, bw=10, latency_ms=5, delay='5ms' )





        ## access - aggregation links

        ### left switches

        self.addLink( s4, s2, cls=TCLink, bw=10, latency_ms=5, delay='5ms' )

        self.addLink( s4, s2, cls=TCLink, bw=10, latency_ms=5, delay='5ms' )

        ### right switches

        self.addLink( s5, s3, cls=TCLink, bw=10, latency_ms=5, delay='5ms' )

        self.addLink( s5, s3, cls=TCLink, bw=10, latency_ms=5, delay='5ms' )

        ### redundant

        self.addLink( s4, s3, cls=TCLink, bw=10, latency_ms=5, delay='5ms' )

        self.addLink( s5, s2, cls=TCLink, bw=10, latency_ms=5, delay='5ms' )

        ## hosts links

        self.addLink( h1, s4, cls=TCLink, latency_ms=10, delay='10ms' )

        self.addLink( h2, s4, cls=TCLink, latency_ms=10, delay='10ms' )

        self.addLink( h3, s5, cls=TCLink, latency_ms=10, delay='10ms' )

        self.addLink( h4, s5, cls=TCLink, latency_ms=10, delay='10ms' )



        







topos = { 'mytopo': ( lambda: MyTopo() ) }

