#!/bin/bash

# Nombre de la interfaz de red que deseas configurar
interfaz="h1-eth0"

# Nueva dirección IP y máscara de red
ip addr del 10.0.0.1/24 dev h1-eth0

nueva_ip="10.0.0.4"
mascara_red="255.255.255.0"

# Cambiar la dirección IP de la interfaz
ifconfig $interfaz $nueva_ip netmask $mascara_red
