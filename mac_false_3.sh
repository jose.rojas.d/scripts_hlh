#!/bin/bash

# Dirección MAC de origen personalizada
source_mac="00:00:00:00:00:04"

# Dirección IP de origen personalizada
source_ip="10.0.0.4"

# Dirección IP de destino
destination_ip="10.0.0.10"

# Crear el paquete ICMP con dirección MAC y dirección IP personalizadas
packet="Ether(src='$source_mac')/IP(src='$source_ip', dst='$destination_ip')/ICMP()"

# Instalar Scapy si no está instalado
# (Asegúrate de que esta parte del script esté descomentada solo para la primera ejecución)

# sudo apt-get update
# sudo apt-get install -y python3-scapy

# Enviar el paquete utilizando Scapy cada 1 segundo

seconds=0
while true; do
  echo "Segundos transcurridos: $seconds"
  python3 -c "from scapy.all import sendp, Ether, IP, ICMP; sendp($packet, iface='h1-eth0')"
  sleep 1
  ((seconds++))
done
