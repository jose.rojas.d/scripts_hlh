import subprocess
import time

def enviar_ping(host_destino):
    while True:
        # Ejecutar el comando de ping y capturar la salida
        resultado_ping = subprocess.run(['ping', '-c', '1', host_destino], stdout=subprocess.PIPE)

        # Verificar si el host está alcanzable
        if resultado_ping.returncode == 0:
            print(f'Ping a {host_destino} exitoso.')
        else:
            print(f'Host {host_destino} no responde. Deteniendo el envio de pings.')
            break

        # Esperar antes de enviar el siguiente ping (puedes ajustar el tiempo según tus necesidades)
        time.sleep(1)

if __name__ == "__main__":
    # Llama a la función para enviar pings
    enviar_ping('10.0.0.4')
